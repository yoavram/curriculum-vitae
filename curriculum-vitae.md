% Curriculum Vitae
% Yoav Ram
% June 2016

## Personal Information

Name
  ~ Yoav Ram

Phone
  ~ +972-54-5383136

Email
  ~ yoavram@post.tau.ac.il

Website
  ~ [www.yoavram.com](http://www.yoavram.com/) / [blog.yoavram.com](http://blog.yoavram.com/)

## Education

*   **Postdoctoral fellow**, Hadany lab, Tel-Aviv University

*   **Ph.D.** 2010 - 2016

    * *Institution:* Tel-Aviv University
    * *Advisor:* Prof. Lilach Hadany
    * *Ph.D. title:* The Evolution of Stress-Induced Mutagenesis: Causes and Consequences
    * Submitted February 2016

*   **M.Sc.** 2010-2012 (direct Ph.D. track)

    Theoretical and Computational Biology, Tel-Aviv University

*   **B.Sc.** 2007-2010

    Biology and Mathematics *magna cum laude*, Tel-Aviv University

## Publications
*   **Ram, Y.** and Hadany, L. (2015) The probability of improvement in Fisher's geometric model: a probabilistic approach. *Theoretical Population Biology*, 99: 1-6. DOI: [10.1016/j.tpb.2014.10.004](http://dx.doi.org/10.1016/j.tpb.2014.10.004)
*   **Ram, Y.** and Hadany, L. (2014) Stress-induced mutagenesis and complex adaptation. *Proceeding of the Royal Society B*. 281: 20141025. DOI: [10.1098/rspb.2014.1025](http://dx.doi.org/10.1098/rspb.2014.1025)
*   Gueijman, A., Ayali, A., **Ram, Y.**, and Hadany, L. (2013) Dispersing away from bad environments: The evolution of fitness-associated dispersal (FAD) in homogeneous environments. *BMC Evol. Biol.*, 13:125. DOI: [10.1186/1471-2148-13-125](http://dx.doi.org/10.1186/1471-2148-13-125) (OA)
*   **Ram, Y.** and Hadany, L. (2012) The evolution of stress-induced hypermutation in asexual populations. *Evolution*, 66: 2315-2328. DOI: [10.1111/j.1558-5646.2012.01576.x](http://dx.doi.org/10.1111/j.1558-5646.2012.01576.x)

### In progress
*   **Ram, Y.**, and Hadany, L. Condition-dependent sex: who does it, when, and why?. *Submitted*.
*   Geffen, E., Hadany, L., **Ram, Y.**, and Gafny, S. Genetic diversity in edge populations: A complementary view. *Submitted*.

## Software & Datasets
*   **Ram Y.**, Hadany L. (2014) Data from: Stress-induced mutagenesis and complex adaptation. *Dryad Digital Repository*. DOI: [10.5061/dryad.3066j](http://dx.doi.org/10.5061/dryad.3066j).
*   **Ram, Y.**, Hadany, L. (2014) proevolution simulation: Version Charles. *ZENODO*.  DOI: [10.5281/zenodo.11401](http://dx.doi.org/10.5281/zenodo.11401).
*   **Ram, Y.**, Guiejman, A. and Hadany L. (2014). proevolution simulation: Version Dobzhansky. *ZENODO*. DOI: [10.5281/zenodo.11400](http://dx.doi.org/10.5281/zenodo.11400).

## Presentations

2016
:   *The Evolution of Stress-Induced Mutagenesis* - [seminar](https://github.com/yoavram/Oranim2016) at the Department of Biology and the Environment, Faculty of Natural Sciences, University of Haifa-Oranim, May 22.
:   *The Evolution of Stress-Induced Mutagenesis: Causes and Consequences* - [PhD seminar](https://github.com/yoavram/PhDSeminar2016) at the Department of Molecular Biology and Ecology of Plants, Tel-Aviv University, April 6.
:   *Microbial Growth in a Mixed Culture* - Poster at ISM (Israel Society for Microbiology) annual meeting in Bar-Ilan, February 17-18.
:	  *The Population Genetics of Stress-Induced Mutation* - seminar at the IGC, Portugal, January 14.

2015
:	  Poster at the *first Pearl Seiden International meeting in Life Sciences*, Technion, Haifa, Israel, December 9 - 10.
:   **Oral presentation** at the **Gordon Research Conference** *on Biological Mechanisms in Evolution*, Stonehill College, Easton, MA, USA, June 28 - July 3.
:	  Poster at the *Forecasting Evolution?* SFB 680 conference in Lisbon, Portugal, July 8 - 11.
:	  *The Population Genetics of Stress-Induced Mutation* - seminar at the IST, Austria, June 25.
:   *Stress-induced mutagenesis & complex adaptation* - **Short oral presentation** at the Two2Many Systems Biology Conference, Weizmann Institute, March.
:   Oral presentation at the Quantitivate and Evolutionary Biology Seminar, Tel-Aviv University, March.
:   *On the role of stress-induced mutagenesis in evolution* - Poster at the Israeli Genetics Society annual meeting, Weizmann Institute, February.

2014
:   *On the role of stress-induced mutagenesis in evolution* - Poster at ISM (Israel Society for Microbiology) annual meeting, Haifa, April.
:   *Stress, mutation, evolution, and mathematical models* - Presentation at the Astrophysics student journal club at Tel-Aviv University, March.
:   *Modeling the evolution of the mutation rate* - Presentation at the Topics in Theoretical and Mathematical Biology Seminar at Tel-Aviv University, March.
:   *The rate of complex adaptation with and without stress-induced mutagenesis* - Poster at FISEB/ILANIT 2014, Eilat, February.

2013
:   Seminar presentation at the Pupko Lab at Tel-Aviv University, November.
:   Seminar presentation at the Philisophy of Biology Group at the Humanities Faculty, Tel-Aviv University, October.
:   *Adaptedness, Adaptability and Stress-Induced Mutagenesis* - Poster at *PDEs and Dynamical Systems in Biology* conference in Bar-Ilan University, October 1.
:   *Adaptedness, Adaptability and Stress-Induced Mutagenesis* - Poster at **ESEB 2013** in Lisbon, Portugal, August 19-24.
:   *Stress-induced Mutagenesis and the Evolution of Complex Traits* - Poster at the **Gordon Research Conference** *on Biological Mechanisms in Evolution*, Stonehill College, Easton, MA, USA, June 2-6.
:   *The Role of Stress-Induced Mutation in the Emergence of Complex Adaptations* - **Oral presentation** at the *5th Anual SIDEER Graduate Symposium*, Ben-Gurion University of the Negev, Sede-Boqer Campus, March 20.

2012
:    *Evolutionary Causes and Consequences of Stress-Induced Hypermutation* - **Oral presentation** at *The 1st Graduate Students' Conference in Genetics, Genomics and Evolution*, Ben-Gurion University of the Negev, September 23. DOI: [10.6084/m9.figshare.96049](http://dx.doi.org/10.6084/m9.figshare.96049).
:    Seminar presentation at the Pilpel Lab at the Weizmann Institute of Science, June.
:    *The Evolution of Stress-Induced Mutagenesis in Finite and Infinite Populations* - **Oral presentation at *PopGroup 45*** in Nottingham, UK, January. DOI: [10.6084/m9.figshare.95939](http://dx.doi.org/10.6084/m9.figshare.96049).

2011
:    Seminar presentation at the Eldar Lab at Tel-Aviv University, December.
:    Seminar presentation at the Department of Molecular Biology and Plants M.Sc. Seminar at Tel-Aviv University, November.
:    *Rise and Fall of Mutator Bacteria* - Seminar presentation at the *BioMath Student Journal Club*, Tel-Aviv University, May. DOI: [10.6084/m9.figshare.95940](http://dx.doi.org/10.6084/m9.figshare.95940).
:    *The Evolution of Stress-Induced Hypermutation* - Poster at **ESEB 2011** in Tubingen, Germany, August. See: [F1000 Posters 2011, 2: 1484](http://f1000.com/posters/browse/summary/2211).

2010
:    *The Evolution of Stress-Induced Hypermutation* - Poster at **SMBE 20108** in Lyon, France, July.
:    *The Evolution of Stress-Induced Hypermutation* - Poster at *MMSB 2010* in Tel-Aviv, Israel – won the **Best poster award**.

## Awards

2016
:    Dean’s Award for Excellence in Teaching at the Faculty of Engineering, Tel-Aviv University.

2015
:	 The *Israel Society for Microbiology Travel Award* to *Forecasting Evolution?* in Lisbon, Portugal.
:	 The *Anat Karuskopf Fund Travel Award* to *Forecasting Evolution?* in Lisbon, Portugal.
:    Travel award from the *Manna Institute for Plants BioSciences* to *Forecasting Evolution?* in Lisbon, Portugal.

2014
:    The Ministry of Science and Technology International Education Scholarship to travel to the 2015 **Gordon Research Conference** *on Biological Mechanisms in Evolution*.
:    Research achievements scholarship from the Life-Science faculty, Tel-Aviv University.

2013
:    Fellowship from the *Manna Program in Food Safety and Security* for 2013-14.
:    Travel award from the *Manna Institute for Plants BioSciences* to *ESEB 2013* in Lisbon, Portugal.

2012
:    Research achievements scholarship from the Life-Science faculty, Tel-Aviv University.
:    Registration and accommodation waiver (sponsored by Cambridge University Press) for *PopGroup45* in Nottingham, UK.
:    Crowd-funded travel expenses for *PopGroup 45* via *The #SciFund Challenge*.

2011
:    SSE International Travel Award to *ESEB 2011* in Tubingen, Germany.

2010
:    Direct Ph.D. scholarship from the Department of Molecular Biology and Ecology of Plants, Tel-Aviv University, 2010-2015.
:    Undergraduate Diversity Mentoring Program award at *SMBE 2010* in Lyon, France.
:    Best poster award at *MMSB 2010* in Tel-Aviv, Israel.

2009
:    The *Amos de-Shalit* Ulpana for excellent students in Life-Sciences at the Weizmann Institute of Science.

## Peer-Reviews

2016
:	   ReScience

2015
:    Theoretical Population Biology
:	   Evolution

2014
:    PLOS ONE

2012
:    Proceeding of the Royal Society B

2011
:    Integrative Zoology

## Teaching

Spring 2015
:    Python Programming for Biologists (0411-3122, Teacher) - new graduate students course
:	   Evolution (0455-2536, TA)

Fall 2014
:    Python Programming for Engineers (0509-1820, Teacher) - received Dean’s Award for Excellence in Teaching

Summer 2014
:    Gentle Introduction to Programming (preparation course for 1st year CS students)
:    Introduction to Programming for Engineers (preparation course for 1st year eng. students)

Fall 2013
:    Python Programming for Engineers (0509-1820, Teacher)

Summer 2013
:    Gentle Introduction to Programming (preparation course for 1st year CS students)
:    Introduction to Programming for Engineers (preparation course for 1st year eng. students)

Spring 2013
:    Extended Introduction to Computer Science (0368-1105, TA)

Fall 2012
:    Python Programming for Engineers (0509-1820, TA)

Spring 2012
:    Evolution (0455-2536, TA)

Fall 2011
:    Python Programming for Engineers (0509-1821, TA)

2011-2014
:    Co-organizing the *BioMath Student Journal Club* at Tel-Aviv University

## Conferences

2016
:    PyCon Israel 2016, Tel Aviv, May.
:    Israeli Society for Microbiology (ISM) annual meeting in Bar-Ilan, February.

2015
:	 The *first Pearl Seiden International meeting in Life Sciences*, Technion, Haifa, Israel, December.
:    **Gordon Research Conference** *on Biological Mechanisms in Evolution* in Stonehill College, Easton, MA, USA, June.
:	 *Forecasting Evolution?* SFB 680 conference in Lisbon, Portugal, July.
:    *Two2Many* Systems Biology Conference, Weizmann Institute, March.
:    Israeli Genetics Society annual meeting in Weizmann Institute, February.
:    Israeli Society for Microbiology (ISM) annual meeting in Bar-Ilan, February.

2014
:    Israeli Society for Microbiology (ISM) annual meeting in Haifa, April.
:    The *Federeation of the Israeli Societies for Experimental Biology (ILANIT)* congress in Eilat, Israel, February.

2013
:    *PDEs and Dynamical Systems in Biology* conference in Bar-Ilan University, October.
:    *The 14th Congress of the European Society for Evolutionary Biology* (ESEB) in Lisbon, Portugal, August.
:    **Gordon Research Conference** *on Biological Mechanisms in Evolution* in Stonehill College, Easton, MA, USA, June.
:    *The 5th Annual SIDEER Graduate Symposium - The Emergence of Design in Nature* in Ben-Gurion University of the Negev, Sede-Boqer Campus, March.

2012
:    *The 1st Graduate Students' Conference in Genetics, Genomics and Evolution* in Ben-Gurion University of the Negev, September.
:    *The 45th Population Genetics Group* meeting in Nottingham, UK, January.

2011
:    *The 13th Congress of the European Society for Evolutionary Biology* (ESEB) in Tubingen, Germany, August.

2010
:    *Learning, Decision Making and Evolutionary Theory: Can We Bridge the Gap?* in Kfar-Blum, Israel, November.
:    *Theoretical Models in Ecology, Evolution and Behavior: Recent Advances and Conceptual Issues* at the Hebrew University in Jerusalem, Israel, October.
:    *The annual meeting of the Society for Molecular Biology and Evolution* (SMBE) in Lyon, France, July.
:    *Mathematical Methods in Systems Biology* in Tel-Aviv University, Israel, January.

2009
:    *From Darwin to Evo-Devo Symposium* in the Technion, Haifa, Israel, October.
:    *Evolution in Jerusalem: A Workshop in Celebration of Darwin’s ‘The Origin of Species’* in the Hebrew University, Jerusalem, Israel, October.

## Public Outreach

### Publications

*   Even-Tov, E., Obolski, U., Gueijman, A., **Ram, Y.**, Hadany, L. (2010) The Evolutionary Riddle of Sexual Reproduction. *Galileo, an Israeli journal for popular science*: June, 32-41

### Presentations

* **How to Study Evolution Using Scientific Python** - [contributed talk](http://il.pycon.org/2016/about/speakers.html#61aa36d93e7cee64) at [PyCon Israel 2016](http://il.pycon.org/2016), Tel Aviv, May 2 - 3, 2016
* **Evolution and mutation in stressful times** - the evolution series in Tel-Aviv University's Botanical Gardens, May 16, 2013.
